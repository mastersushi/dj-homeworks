import urllib.parse
from csv import DictReader
from functools import lru_cache

from django.conf import settings
from django.http import HttpResponseNotFound
from django.shortcuts import render, redirect
from django.urls import reverse
from django.core.paginator import Paginator


def index(request):
    return redirect(reverse(bus_stations))


def bus_stations(request):
    bus_station_pages = Paginator(__get_bus_stations(), 10)

    page_number = int(request.GET.get('page', 1))
    if page_number < 1 or page_number > bus_station_pages.num_pages:
        return HttpResponseNotFound('Not found')

    current_page = bus_station_pages.page(page_number if page_number else 1)
    return render(request, 'index.html', context={
        'bus_stations': current_page.object_list,
        'current_page': current_page.number,
        'prev_page_url': __build_url(current_page.previous_page_number()) if current_page.has_previous() else None,
        'next_page_url': __build_url(current_page.next_page_number()) if current_page.has_next() else None,
    })


@lru_cache
def __get_bus_stations():
    with open(settings.BUS_STATION_CSV, newline='', encoding='cp1251') as bus_stations_file:
        bus_stations_reader = DictReader(bus_stations_file)
        return list(bus_stations_reader)


def __build_url(page_num):
    return reverse('bus_stations') + '?' + urllib.parse.urlencode({'page': page_num})

