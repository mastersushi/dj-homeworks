from collections import Counter

from django.http import HttpResponseNotFound
from django.shortcuts import render

# fixme: save to the persistent storage
counter_show = Counter()
counter_click = Counter()


def index(request):
    from_landing = request.GET.get('from-landing')

    if from_landing is None or from_landing == 'original':
        counter_click.update({'original': 1})
    elif from_landing == 'test':
        counter_click.update({'test': 1})

    return render(request, 'index.html')


def landing(request):
    ab_test_arg = request.GET.get('ab-test-arg')

    if ab_test_arg is None or ab_test_arg == 'original':
        counter_show.update({'original': 1})
        return render(request, 'landing.html')
    elif ab_test_arg == 'test':
        counter_show.update({'test': 1})
        return render(request, 'landing_alternate.html')

    return HttpResponseNotFound('<h1>Страница не найдена</h1>')


def stats(request):
    return render(request, 'stats.html', context={
        'test_conversion': __format_conversion(counter_show['test'], counter_click['test']),
        'original_conversion': __format_conversion(counter_show['original'], counter_click['original'])
    })


def __format_conversion(visitors_num, clickers_num):
    if visitors_num != 0:
        return "{:.2f}".format(clickers_num * 1.0 / visitors_num)

    return 'N/A'
